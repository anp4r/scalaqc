#!/bin/bash -l
# maxPartition reduced from 128 MB to 96 MB 100663296
# maxPartition reduced from 128 MB to 64 MB 67108864
# timeout increased from 120s to 360s
# timeout increased from 360s to 480s
module load python3/3.7.10
module load spark/3.1.2
module list
for chrom in {{1..22},X,Y,M}
do
  vcf_input="/rprojectnb/adsp-charge/achilleas/multi_allelic_vars/data/intermediate/split_filtered_vcfs/split_chr${chrom}.ALL.vcf.gz"
  vcf_output_prefix="/rprojectnb/adsp-charge/achilleas/multi_allelic_vars/data/intermediate/flag_files/split_chr${chrom}_info_only_both_flags"

  time spark-submit --class "SimpleApp" \
               --deploy-mode  client \
               --packages io.projectglow:glow-spark3_2.12:1.0.1 \
               --conf spark.hadoop.io.compression.codecs=io.projectglow.sql.util.BGZFCodec \
               --conf spark.driver.cores=8 \
               --conf spark.network.timeout=480s \
               --conf spark.sql.files.maxPartitionBytes=67108864\
               --master yarn \
               --driver-memory 8G \
               --executor-memory 4G \
               --executor-cores 3 \
               --num-executors 196 \
               ../simple-project_2.12-1.0.jar "file://${vcf_input}" \
                                      "file://${vcf_output_prefix}" \
                                      "file:///rprojectnb/adsp-charge/achilleas/test/compact_filtered_header_with_abhet_no_gts.txt"
done

