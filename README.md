# Generate ABHET and VFLAGS for VCFs

- The scala code uses spark and the glow library to generate a file with the ABHET and VFLAGS options
that can be added to the VCF file.

- The generated jar file is included in case you don't want to recompile the package

- The shell script run.sh has an example of running the code on our local hpc.

## Notes: 
1. The code does per genotype filtering using the GQ and DP genotype fields before doing any calculations. Genotypes with with DP < 10 or GQ < 20 are considered as missing in the calculations.
2. ABHET can have the value nan when there are no heterozygote calls. There might be the need to replace that to be consistent with other QC.




