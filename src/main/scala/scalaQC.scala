import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf  
import org.apache.spark.sql.functions.col  
import org.apache.spark.sql.functions.arrays_zip  
import org.apache.spark.sql.functions.array_contains  
import org.apache.spark.sql.types._
import scala.collection.mutable.WrappedArray
import io.projectglow.Glow

object SimpleApp {
  import aux.AuxFunction.{vflagPure, abHetPure, filterGenotypesPure, updatePhasePure}
  def main(args: Array[String]){
    val spark = SparkSession.builder.appName("Simple Application").getOrCreate()
    val sess = Glow.register(spark)
    val path = if (!args.isEmpty) args(0) else throw new Exception("No argument was given")
    val path_out_prefix = if (args.length >= 2) args(1) else throw new Exception("No argument was given")
    val vcf_header = if (args.length >= 3) args(2) else throw new Exception("No argument was given")
    val df = sess.read.format("vcf").load(path)
    val mySchema = ArrayType(new StructType().
                             add(StructField("sampleId", StringType, true)).
                             add(StructField("phased", BooleanType, true)).
                             add(StructField("calls", ArrayType(IntegerType))))
    val infoToDrop  = df.columns.filter(_.startsWith("INFO_"))

    sess.udf.register("vflag", vflag)
    sess.udf.register("abhet", abhet)
    sess.udf.register("filterGenotypes", filterGenotypes)
    sess.udf.register("updatePhase", updatePhase)

    val flaggedOutput = df.withColumn("filteredCalls", filterGenotypes(col("genotypes.calls"), col("genotypes.depth"), col("genotypes.conditionalQuality"))).
                           withColumn("INFO_VFLAGS", vflag(col("filteredCalls"), col("filters"), col("genotypes.depth"))).
                           withColumn("INFO_ABHET", abhet(col("filteredCalls"), col("genotypes.alleleDepths"))).
                           withColumn("updatedPhased", updatePhase(col("filteredCalls"), col("genotypes.phased"))).
                           drop(col("genotypes")).
                           drop(col("filteredCalls")).
                           drop(infoToDrop:_*)
                    
   flaggedOutput. write.
                    option("vcfHeader",vcf_header).
                    format("bigvcf").
                    save(path_out_prefix + ".vcf.bgz")
 
  }

  val vflag = udf(vflagPure _)
  val abhet = udf(abHetPure _)
  val filterGenotypes = udf(filterGenotypesPure _)
  val updatePhase = udf(updatePhasePure _)
}

package aux {
  object AuxFunction {  
  case class MissingInfo(missingRate: Double, isMonomorphic: Boolean, allMissing: Boolean)


  def updatePhasePure( genotypes: Seq[WrappedArray[Int]],
                        phased: Seq[Boolean] ) : Option[Seq[Boolean]]= {
                 for {
                   gts <- Option(genotypes)
                   p <- Option(phased)
                 } yield {
                   p zip gts map { 
                     x => if (x._2.contains(-1)) false else x._1 }
                 }
  }



  def auxFilter(genotype: Option[WrappedArray[Int]],
                seqDepth: Option[Integer],
                genotypeQuality: Option[Integer]) : Option[WrappedArray[Int]] = {
         
                  val test = for {
                          gq <- genotypeQuality
                          dp <- seqDepth 
                         } yield {
                          dp >= 10 && gq >= 20
                         }

                  test match {
                    case Some(p) => if (p) genotype else Some(Array(-1,-1))
                    case None => Some(Array(-1,-1))
                  }
                  
                }


  def filterGenotypesPure(genotypes: Seq[WrappedArray[Int]],
                      sequenceDepths: Seq[Integer],
                      genotypeQualities: Seq[Integer]) : Option[Seq[Option[WrappedArray[Int]]]] = {
                        for {
                          gts <- Option(genotypes)
                          dps <- Option(sequenceDepths)
                          gqs <- Option(genotypeQualities)
                        } yield {
                          val zippedVals =  ((gts map { nullableToOption(_) }) zip 
                                               (dps map { nullableToOption(_) }) zip
                                               (gqs map { nullableToOption(_) }) ) 
                          zippedVals map { case(((gt,dp),gq)) => auxFilter(gt,dp,gq)}
                        }
                      }


  def nullableToOption[T] ( x : T) : Option[T] = {
     x match {
       case null => None
       case v  => Option(v)
     }

  }




  def getMissingInfo(genotypes :Seq[WrappedArray[Int]]) : Option[MissingInfo] = {
    Option(genotypes).map { xs => 
        val total  = xs.size
        val notMissing = xs.filterNot(_.contains(-1))
        val numMissing = total - notMissing.size 
        val allMissing = (total == numMissing)
        val missingRate = numMissing.toDouble / total.toDouble
        val isMonomorphic = if (allMissing) {
                               false }
                          else {
                           val firstElement = notMissing(0)
                           notMissing.forall(firstElement == _)
                          }
        MissingInfo(missingRate,isMonomorphic, allMissing)
    }
  }
 
  def abHetPure(genotypes: Seq[WrappedArray[Int]],
                adCounts: Seq[WrappedArray[Int]]
                ): Option[Double] = {
        val hetAD: Option[Seq[WrappedArray[Int]]] = for {
          gts <- Option(genotypes)
          ads <- Option(adCounts)

        }
        yield ( gts.
                zip(ads).
                filterNot(x => x._1.contains(-1)).
                filter(x => x._1(0) != x._1(1)).
                map(_._2)
              )
        
        hetAD.map(_.foldLeft[(Double, Double)]((0,0)) ((acc, el) => (acc._1 + el(0), acc._2 + el(0) + el(1))))
             .map { case (num, den) => num/den }
  }

  def meanDepth(genotypes: Seq[WrappedArray[Int]],
                sequenceDepths: Seq[Integer]) : Option[Double] = {
                 val dps =  (sequenceDepths.map{x=> Option(x)}).zip(genotypes).
                                 filterNot {x => x._2.contains(-1)}.
                                 map(x => x._1)

                 val sumDP = dps.foldLeft[Option[Double]](Some(0)) ((acc, el) => acc.flatMap( x => el.map( y => y + x)))
                 sumDP.map( _ / dps.size)
  }


  def vflagPure(genotypes: Seq[WrappedArray[Int]],
                filterTags: Seq[String],
                sequenceDepths: Seq[Integer]) : Option[String] = {
                 val  myMissingInfo = getMissingInfo(genotypes)
                 val  filterT = toMaybeString(Option(filterTags),(x: Seq[String]) => !x.contains("PASS"), "1")
                 val  allMissing =  toMaybeString(myMissingInfo.map(_.allMissing), identity[Boolean], "2")
                 val  isMonomorphic = toMaybeString(myMissingInfo.map(_.isMonomorphic), identity[Boolean],"3")
                 val  missingRate =  toMaybeString(myMissingInfo.map(_.missingRate),(x: Double) => x >= 0.2,"4")
                 val depthCutoff = 500
                 val  meanDP = toMaybeString(meanDepth(genotypes,sequenceDepths), (x: Double) => x > depthCutoff, "5")
                 val res: String = Seq(filterT, allMissing, isMonomorphic, missingRate, meanDP).collect{case Some(s) => s}.mkString(",")
                if (res.isEmpty) Some("0") else Some(res)
  }

  def identity[T](x: T) : T = {
    x
  }
  def toMaybeString[T](v: Option[T], p: T => Boolean, x: String): Option[String] = v match {
    case None => None
    case Some(s) => if (p(s)) Some(x) else None
  }
  }

}

